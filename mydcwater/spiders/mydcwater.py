# -*- coding: utf-8 -*-

from __future__ import division, absolute_import, unicode_literals
from scrapy import Spider
import re
from selenium import webdriver
import os
from time import sleep
import requests


class MyDcWaterSpider(Spider):
    name = "mydcwater"
    start_urls = [
        'https://www.mydcwater.com/DCWSSP/Index.aspx'
    ]

    passed_vals = []

    def __init__(self, username=None, password=None, download_directory=None, *args, **kwargs):
        super(MyDcWaterSpider, self).__init__(*args, **kwargs)
        self.user_name = username
        self.password = password
        self.download_directory = download_directory if download_directory else 'C:/Users/Dev/Downloads/mydcwater/'
        
        cwd = os.getcwd()
        opt = webdriver.ChromeOptions()
        opt.add_argument('--headless')
        self.driver = webdriver.Chrome(executable_path='{}/chromedriver.exe'.format(cwd), chrome_options=opt)

        with open('scrapy.log', 'r') as f:
            self.logs = [i.strip() for i in f.readlines()]
            f.close()

    def login(self):
        while True:
            try:
                user_email = self.driver.find_element_by_xpath(
                    '//form//input[@id="body_content_txtUsername"]'
                )
                user_email.send_keys(self.user_name)
                password = self.driver.find_element_by_xpath(
                    '//form//input[@id="body_content_txtPassword"]'
                )
                password.send_keys(self.password)
                btn_login = self.driver.find_element_by_xpath(
                    '//form//input[@id="body_content_btnLogin"]'
                )
                btn_login.click()
                break
            except:
                sleep(10)
                continue

        while True:
            try:
                self.driver.find_element_by_xpath('//table[@class="MenuTable"]//td[@class="MenuItem" and contains(@onclick, "LedgerHistory.aspx")]')
                break
            except:
                sleep(10)
                continue

    def parse(self, response):
        
        self.driver.get(response.url)
        self.login()
        current_account = ''

        while True:
            try:
                if self.driver.current_url != 'https://www.mydcwater.com/DCWSSP/LedgerHistory.aspx':
                    self.driver.get('https://www.mydcwater.com/DCWSSP/LedgerHistory.aspx')

                val = self.driver.find_element_by_xpath(
                    '//select[contains(@name, "headerAccountSelector")]/option[@selected]'
                ).get_attribute('value')

                if current_account and val != current_account:
                    sleep(10)
                    continue

                account_title = self.driver.find_element_by_xpath(
                    '//select[contains(@name, "headerAccountSelector")]/option[@selected]'
                ).text

                links = self.driver.find_elements_by_xpath(
                    '//table[contains(@id, "body_content_LedgerData")]//td[@class="LinkCell" and position()=3]/a[text()="Charge"]'
                )

                cookies = self.driver.get_cookies()
                for l in links:
                    pdf_link = l.get_attribute('href')
                    if not pdf_link:
                        continue
                    bill_date = re.search(r'(\d+)', pdf_link)
                    bill_date = bill_date.group(1) if bill_date else None
                    title = '{}-{}'.format(account_title, bill_date)
                    if title not in self.logs:
                        yield self.download_page(pdf_link, title, account_title, bill_date, cookies)

                if val not in self.passed_vals:
                    self.passed_vals.append(val)

                options = self.driver.find_elements_by_xpath(
                    '//select[contains(@name, "headerAccountSelector")]/option[not(@selected)]'
                )
                finished = False
                for opt in options:
                    if opt.get_attribute('value') not in self.passed_vals:
                        current_account = opt.get_attribute('value')
                        opt.click()
                        finished = True
                        break
                if not finished:
                    break
            except Exception as e:
                sleep(10)
                continue

    def download_page(self, pdf_link, title, account_title, bill_date, cookies):
        s = requests.Session()
        for cookie in cookies:
            s.cookies.set(cookie['name'], cookie['value'])

        raw_pdf = s.get(pdf_link)

        try:
            if not raw_pdf.text.startswith("%PDF"):
                return {}
        except:
            return {}

        file_name = '{}{}.pdf'.format(self.download_directory, title)

        with open(file_name, 'wb') as f:
            f.write(raw_pdf.content)
            self.logger.info('{} is downloaded successfully'.format(account_title))
            f.close()
        self.write_logs(title)
        return {
            'file_name': file_name,
            'file_url': pdf_link,
            'account_number': account_title,
            'bill_date': bill_date
        }

    def date_to_string(self, d):
        d = d.split('/')
        return ''.join([i.zfill(2) for i in d])

    def write_logs(self, bill_id):
        with open('scrapy.log', 'a') as f:
            f.write(bill_id + '\n')
            f.close()
        self.logs.append(bill_id)
